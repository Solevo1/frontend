import {
  map, orderBy, filter, forEach, toPairs,
} from 'lodash';

const codes = require('../data/codes.json');

const courses = [null, 'maths', 'history', 'biology', 'psychology', 'PE', 'geography', 'physics'];

const randomIndex = (arrayLength) => Math.floor(Math.random() * arrayLength);

const randomString = (length) => {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(randomIndex(characters.length));
  }
  return result;
};

const exportArr = {

  normalizedJoin(mockArray, additionalArray) {
    const formattedArray = map(mockArray, (item, index) => ({
      gender: item.gender,
      title: item.name.title,
      full_name: `${item.name.first} ${item.name.last}`,
      city: item.location.city,
      state: item.location.state,
      country: item.location.country,
      postcode: item.location.postcode,
      coordinates: item.location.coordinates,
      timezone: item.location.timezone,
      email: item.email,
      b_date: item.dob.date,
      age: item.dob.age,
      phone: item.phone,
      picture_large: item.picture.large,
      picture_thumbnail: item.picture.thumbnail,
      id: index.toString(),
      favorite: false,
      course: courses[randomIndex(courses.length)],
      bg_color: '#'.concat(randomIndex(16777215).toString(16)),
      note: randomString(randomIndex(10)),
    }
    ));
    if (additionalArray) {
      forEach(additionalArray, (elem) => {
        if (!formattedArray.find((item) => item.full_name === elem.full_name)) {
          const copy = { ...elem };
          if (!elem.age) {
            copy.age = randomIndex(100);
          }
          if (!elem.email) {
            copy.email = 'example@gmail.com';
          }
          if (!elem.phone) {
            copy.phone = '228-337';
          }
          copy.favorite = true;
          formattedArray.push(copy);
        }
      });
    }
    return formattedArray;
  },

  validation(rawObject) {
    const strings = ['full_name', 'gender', 'note', 'state', 'city', 'country'];
    const invalidFields = {};
    forEach(toPairs(rawObject), ([key, value]) => {
      if (strings.includes(key)) {
        if (typeof (value) !== 'string') {
          invalidFields[key] = value;
        }
      }
    });
    if (typeof (rawObject.age) !== 'number') {
      invalidFields.age = rawObject.age;
    }
    if (rawObject.phone) {
      forEach(codes.country, (el) => {
        if (el.eng === rawObject.country) {
          const lastIndex = rawObject.phone.indexOf('-') !== -1 ? rawObject.phone.indexOf('-') : rawObject.phone.indexOf(' ');
          const code = rawObject.phone.slice(0, lastIndex);
          if (el.code.substr(1) !== code) {
            invalidFields.phone = rawObject.phone;
          }
        }
      });
    }
    if (rawObject.email) {
      const re = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
      if (!rawObject.email.match(re)) {
        invalidFields.email = rawObject.email;
      }
    }
    return invalidFields;
  },

  filtration(array, params) {
    let formattedArray = array.slice();
    const copy = { ...params };
    forEach(toPairs(copy), ([key, value]) => {
      if (value === '' || value === 0) {
        delete copy[key];
      }
    });
    formattedArray = filter(array, copy);
    return formattedArray;
  },

  sorting(array, params) {
    let sortedArray;
    forEach(toPairs(params), ([key, value]) => {
      if (!value) {
        sortedArray = orderBy(array, key, 'desc');
      } else {
        sortedArray = orderBy(array, key, 'asc');
      }
    });
    return sortedArray;
  },

  search(array, param) {
    let searchResultArray = array.slice();
    const parametr = Object.entries(param)[0];
    if (Number(parametr[1])) {
      searchResultArray = filter(searchResultArray, param);
    } else {
      searchResultArray = filter(searchResultArray, (el) => (
        el[parametr[0]].toLowerCase().includes(parametr[1].toLowerCase())
      ));
    }
    return searchResultArray;
  },

  percentage(array, params) {
    const newArr = this.filtration(array, params);
    const persent = (newArr.length * 100) / array.length;
    return persent;
  },

};

export default exportArr;
