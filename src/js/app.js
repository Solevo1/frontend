import Chart from 'chart.js';
import dayjs from 'dayjs';
import functions from './operations';
import '../scss/style.scss';

let teachers = [];

const { L } = window;
const $teachersList = document.querySelector('.teachers-list');
const $favoriteStar = document.querySelector('.star');
const $statisticsTableBody = document.querySelector('.statistics-table-body');
const $teachersSection = document.querySelector('#teachers');
const $teacherContent = document.querySelector('.teacher-content');
const $popupImg = document.querySelector('.teacher-content-image');
const $fullname = document.querySelector('.teacher-fullname');
const $location = document.querySelector('.teacher-location');
const $age = document.querySelector('.teacher-age');
const $email = document.querySelector('.teacher-email');
const $phone = document.querySelector('.teacher-phone');
const $about = document.querySelector('.teacher-about');
const $mainPage = document.querySelector('.main-page');
const $teacherInfo = document.querySelector('.teacher_info');
const $favoritesList = document.querySelector('.favorites-list');
let pageNumber = 1;
const pageSize = 10;
let lastPage = 1;
const $firstPage = document.querySelector('.first');
const $prevPage = document.querySelector('.prev');
const $currentPage = document.querySelector('.current');
const $nextPage = document.querySelector('.next');
const $lastPage = document.querySelector('.last');
const $filterSubmitButton = document.querySelector('.input-submit');
const $ageForm = document.querySelector('#filter-age');
const $countryForm = document.querySelector('#filter-country');
const $genderForm = document.querySelector('#filter-gender');
const $filterResetButton = document.querySelector('.input-reset');
const $searchButton = document.querySelector('.submit-button');
const $selectedParam = document.querySelector('.search-select');
const $searchForm = document.querySelector('.search-form');
const $tableName = document.querySelector('.statistics-table-name');
const $tableAge = document.querySelector('.statistics-table-age');
const $tableGender = document.querySelector('.statistics-table-gender');
const $tableCountry = document.querySelector('.statistics-table-country');
const $tableHeader = document.querySelector('.statistics-table-header');
const $inputFullName = document.querySelector('#fullname');
const $inputCountry = document.querySelector('#country');
const $inputCity = document.querySelector('#city');
const $inputPhone = document.querySelector('#phone');
const $inputEmail = document.querySelector('#email');
const $inputGenderMale = document.querySelector('#male');
const $inputAge = document.querySelector('#age');
const $inputColor = document.querySelector('#bgcolor');
const $favoritesCheckbox = document.querySelector('#favorites-checkbox');
const $photoCheckbox = document.querySelector('#photo-checkbox');
const $mapLink = document.querySelector('.map-link');
let $teacherMap = document.querySelector('#teacher-map');
const $arrowRight = document.querySelector('.arrow.right');
const $arrowLeft = document.querySelector('.arrow.left');
const $closeAddForm = document.querySelectorAll('.close')[0];
const $closeInfoForm = document.querySelectorAll('.close')[1];
const $add = document.querySelector('.add');
const $teacherButtonForm = document.querySelectorAll('.add_teacher-button');
const $teacherDataForm = document.querySelector('.data');
const $randomCheckbox = $teacherDataForm.elements[0];
const $genderCheckbox = $teacherDataForm.elements[1];
const $countryCheckbox = $teacherDataForm.elements[3];
const $dataSubmit = $teacherDataForm.elements[5];
const $genderSelect = $teacherDataForm.elements[2];
const $countrySelect = $teacherDataForm.elements[4];
const $listAddButton = document.querySelector('.teachers-list-add');
let canvas1 = document.getElementById('statistics-age-chart');
let canvas2 = document.getElementById('statistics-gender-chart');
const $StatTable = document.querySelector('.statistics-table');
const $bDay = document.querySelector('.bday-remains');

const createElement = (tag, className) => {
  const $tag = document.createElement(tag);
  if (className) {
    $tag.classList.add(className);
  }
  return $tag;
};

const $errorMessage = createElement('div', 'error-message');
$teachersSection.appendChild($errorMessage);
const $table = createElement('tbody');

const findListItem = (fullname, list) => {
  for (let i = 0; i < list.length; i += 1) {
    if (list[i].querySelector('figcaption').innerText === fullname) {
      return list[i];
    }
  }
  return null;
};

const createListItem = (element) => {
  const $imgContainer = createElement('div', 'img-container');
  $imgContainer.addEventListener('click', () => {
    if (element.picture_large) {
      $popupImg.src = element.picture_large;
    } else {
      $popupImg.src = 'images/Anonim.png';
    }
    $fullname.innerText = element.full_name;
    $location.innerText = `${element.city}, ${element.state}, ${element.country}`;
    $age.innerText = `${element.age}, ${
      element.gender === 'male' ? 'M' : 'F'
    }`;
    let daysLeft = dayjs(element.b_date).set('year', dayjs().year()).diff(dayjs(), 'd');
    if (daysLeft < 0) {
      daysLeft = dayjs(element.b_date).set('year', dayjs().year() + 1).diff(dayjs(), 'd');
    }
    $bDay.innerText = `Days until birtday: ${daysLeft}`;
    $email.innerText = element.email;
    $phone.innerText = element.phone;
    $about.innerText = `This lecturer teaches ${element.course}`;
    if (element.favorite) {
      $favoriteStar.src = 'images/full_star.png';
    } else {
      $favoriteStar.src = 'images/star.png';
    }
    $mainPage.classList.toggle('blur');
    $teacherInfo.classList.toggle('displayed');
    $teacherMap.remove();
    $teacherMap = createElement('div', 'teacher-map');
    $teacherMap.id = 'teacher-map';
    $teacherInfo.appendChild($teacherMap);
    const mymap = L.map('teacher-map').setView([element.coordinates.latitude, element.coordinates.longitude], 4);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1IjoiZmxvbWFzdGVyIiwiYSI6ImNrbzcxMHZqbjI4YXoycXBnc2xicTg0MGgifQ.i0jwLiasMnp4ZhG-_1tGFA',
    }).addTo(mymap);
    L.marker([element.coordinates.latitude, element.coordinates.longitude])
      .addTo(mymap);
  });
  if (element.picture_large) {
    const $img = createElement('img');
    $img.src = element.picture_large;
    $imgContainer.appendChild($img);
  } else {
    const $teacherInitials = createElement('div', 'teacher-initials');
    const initials = element.full_name.split(' ');
    $teacherInitials.innerText = initials.map((el) => el[0]).join('.');
    $imgContainer.appendChild($teacherInitials);
  }

  const $figcaption = createElement('figcaption');
  $figcaption.innerText = element.full_name;

  const $figure = createElement('figure');
  $figure.appendChild($imgContainer);
  $figure.appendChild($figcaption);

  const $country = createElement('div', 'countr');
  $country.innerText = element.country;

  const $li = createElement('li');
  $li.appendChild($figure);
  $li.appendChild($country);
  return $li;
};

const createTableRow = (element) => {
  const $rowName = createElement('th');
  $rowName.scope = 'row';
  $rowName.innerText = element.full_name;

  const $tableRow = createElement('tr');
  $tableRow.appendChild($rowName);

  const $rowAge = createElement('td');
  $rowAge.innerText = element.age;
  $tableRow.appendChild($rowAge);

  const $rowGender = createElement('td');
  $rowGender.innerText = element.gender;
  $tableRow.appendChild($rowGender);

  const $rowCountry = createElement('td');
  $rowCountry.innerText = element.country;
  $tableRow.appendChild($rowCountry);

  return $tableRow;
};

const paginateTable = () => {
  let page = Array.prototype.slice.call($table.cloneNode(true).childNodes);
  page = page.slice(pageSize * (pageNumber - 1), pageSize * (pageNumber));
  $statisticsTableBody.innerHTML = '';
  page.forEach((el) => {
    $statisticsTableBody.appendChild(el);
  });
  if (pageNumber === 1) {
    $firstPage.classList.add('disabled');
    $prevPage.classList.add('disabled');
  }
  if (pageNumber === lastPage) {
    $lastPage.classList.add('disabled');
    $nextPage.classList.add('disabled');
  }
  if (pageNumber > 1) {
    $prevPage.classList.remove('disabled');
  }
  if (pageNumber < lastPage) {
    $nextPage.classList.remove('disabled');
  }
  if (pageNumber > 2) {
    $firstPage.classList.remove('disabled');
  }
  if (pageNumber < lastPage - 1) {
    $lastPage.classList.remove('disabled');
  }
};

const initPageData = (dataArray) => {
  const ageGroups = [0, 0, 0, 0, 0];
  const genderGroups = [0, 0];
  $teachersList.innerHTML = '';
  $table.innerHTML = '';
  dataArray.forEach((element) => {
    const $teachersListItem = createListItem(element);
    if (element.favorite) {
      $favoritesList.appendChild($teachersListItem.cloneNode(true));
      $teachersListItem.classList.add('favorite');
    }
    $teachersList.appendChild($teachersListItem);
    $table.appendChild(createTableRow(element));
    if (element.age < 30) {
      ageGroups[0] += 1;
    } else if (element.age < 40) {
      ageGroups[1] += 1;
    } else if (element.age < 50) {
      ageGroups[2] += 1;
    } else if (element.age < 60) {
      ageGroups[3] += 1;
    } else {
      ageGroups[4] += 1;
    }
    if (element.gender === 'male') {
      genderGroups[0] += 1;
    } else {
      genderGroups[1] += 1;
    }
  });
  canvas1.remove();
  canvas1 = createElement('canvas');
  canvas1.id = 'statistics-age-chart';
  $StatTable.before(canvas1);
  canvas2.remove();
  canvas2 = createElement('canvas');
  canvas2.id = 'statistics-gender-chart';
  $StatTable.before(canvas2);
  const ageChart = new Chart(
    canvas1,
    {
      type: 'pie',
      data: {
        labels: [
          '20-30',
          '30-40',
          '40-50',
          '50-60',
          '60+',
        ],
        datasets: [{
          label: 'Age Statistics',
          data: ageGroups,
          backgroundColor: [
            'rgb(0, 255, 0)',
            'rgb(255, 255, 0)',
            'rgb(0, 0, 255)',
            'rgb(255, 0, 0)',
            'rgb(0, 0, 0)',
          ],
          hoverOffset: 4,
        }],
      },
    },
  );
  const genderChart = new Chart(
    canvas2,
    {
      type: 'pie',
      data: {
        labels: [
          'male',
          'female',
        ],
        datasets: [{
          label: 'Gender',
          data: genderGroups,
          backgroundColor: [
            'rgb(0, 0, 255)',
            'rgb(255, 192, 203)',
          ],
          hoverOffset: 4,
        }],
      },
    },
  );
  lastPage = Math.ceil(dataArray.length / pageSize);
  $currentPage.innerText = `Page:${pageNumber}/${lastPage}`;
  paginateTable();
};

const sortTableRow = (node, property) => {
  node.addEventListener('click', (e) => {
    for (let i = 0; i < $tableHeader.childNodes.length; i += 1) {
      if ($tableHeader.childNodes[i] !== e.currentTarget) {
        $tableHeader.childNodes[i].classList.remove('ascending');
        $tableHeader.childNodes[i].classList.remove('descending');
      }
    }
    let sortedArray = teachers.slice();
    if (!node.classList.contains('ascending')) {
      sortedArray = functions.sorting(sortedArray, { [property]: true });
      node.classList.add('ascending');
      node.classList.remove('descending');
    } else {
      sortedArray = functions.sorting(sortedArray, { [property]: false });
      node.classList.add('descending');
      node.classList.remove('ascending');
    }
    $table.innerHTML = '';
    sortedArray.forEach((element) => {
      $table.appendChild(createTableRow(element));
    });
    lastPage = Math.ceil(teachers.length / pageSize);
    $currentPage.innerText = `Page:${pageNumber}/${lastPage}`;
    paginateTable();
  });
};

const doQuery = (query) => {
  fetch(query)
    .then((response) => response.json())
    .then((data) => {
      teachers = functions.normalizedJoin(data.results);
      initPageData(teachers);
    });
};

document.getElementById('addform').addEventListener('submit', (e) => {
  e.preventDefault();
  const newTeacher = {
    full_name: $inputFullName.value,
    country: $inputCountry.value,
    city: $inputCity.value,
    phone: $inputPhone.value,
    email: $inputEmail.value,
    age: Number($inputAge.value),
    gender: $inputGenderMale.checked === true ? 'male' : 'female',
    bg_color: $inputColor.value,
  };
  teachers.push(newTeacher);
  const $teachersListItem = createListItem(newTeacher);
  $teachersList.appendChild($teachersListItem);
  $table.appendChild(createTableRow(newTeacher));
  lastPage = Math.ceil(teachers.length / pageSize);
  $currentPage.innerText = `Page:${pageNumber}/${lastPage}`;
  paginateTable();
  document.querySelector('.main-page').classList.toggle('blur');
  document.querySelector('.add').classList.toggle('displayed');
  fetch('http://localhost:3000/posts', {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    redirect: 'manual',
    referrer: 'no-refferer',
    body: JSON.stringify(newTeacher),
    headers: {
      'Content-Type': 'application/json',
    },
  });
});

$favoritesCheckbox.addEventListener('click', () => {
  for (let i = 0; i < $teachersList.childNodes.length; i += 1) {
    if (!$teachersList.childNodes[i].classList.contains('favorite')) {
      $teachersList.childNodes[i].classList.toggle('hidden_favorites');
    }
  }
});

$photoCheckbox.addEventListener('click', () => {
  for (let i = 0; i < $teachersList.childNodes.length; i += 1) {
    if (!$teachersList.childNodes[i].querySelector('img')) {
      $teachersList.childNodes[i].classList.toggle('hidden_photo');
    }
  }
});

$nextPage.addEventListener('click', () => {
  pageNumber += 1;
  $currentPage.innerText = `Page:${pageNumber}/${lastPage}`;
  paginateTable();
});

$prevPage.addEventListener('click', () => {
  pageNumber -= 1;
  $currentPage.innerText = `Page:${pageNumber}/${lastPage}`;
  paginateTable();
});

$lastPage.addEventListener('click', () => {
  pageNumber = Math.ceil($table.childNodes.length / pageSize);
  $currentPage.innerText = `Page:${pageNumber}/${lastPage}`;
  paginateTable();
});

$firstPage.addEventListener('click', () => {
  pageNumber = 1;
  $currentPage.innerText = `Page:${pageNumber}/${lastPage}`;
  paginateTable();
});

$favoriteStar.addEventListener('click', () => {
  const fullname = $fullname.innerText;
  const $teachersListItem = findListItem(fullname, $teachersList.childNodes);
  if ($favoriteStar.src === 'http://localhost:3001/images/star.png') {
    $favoriteStar.src = 'images/full_star.png';
    $favoritesList.appendChild($teachersListItem.cloneNode(true));
  } else {
    $favoriteStar.src = 'images/star.png';
    const $favoritesListItem = findListItem(fullname, $favoritesList.childNodes);
    $favoritesListItem.remove();
  }
  $teachersListItem.classList.toggle('favorite');
  const teacher = teachers.find((el) => (
    el.full_name === fullname
  ));
  teacher.favorite = !teacher.favorite;
});

$filterSubmitButton.addEventListener('click', (e) => {
  e.preventDefault();
  const filteredTeachersArray = functions.filtration(teachers, {
    age: Number($ageForm.value),
    country: $countryForm.value,
    gender: $genderForm.value,
  });
  if (filteredTeachersArray.length > 0) {
    initPageData(filteredTeachersArray);
    $errorMessage.innerText = '';
  } else {
    $teachersList.innerHTML = '';
    $errorMessage.innerText = 'Unfortunately, there is no teachers with this parametrs';
  }
});

$filterResetButton.addEventListener('click', (e) => {
  e.preventDefault();
  initPageData(teachers);
  $ageForm.value = '';
  $countryForm.value = '';
  $genderForm.value = '';
  $errorMessage.innerText = '';
});

$searchButton.addEventListener('click', (e) => {
  e.preventDefault();
  let searchResultArray = [];
  switch ($selectedParam.value) {
    case 'Name':
      searchResultArray = functions.search(teachers, { full_name: $searchForm.value });
      break;
    case 'Age':
      searchResultArray = functions.search(teachers, { age: Number($searchForm.value) });
      break;
    case 'Note':
      searchResultArray = functions.search(teachers, { note: $searchForm.value });
      break;
    default:
  }
  if (searchResultArray.length > 0) {
    initPageData(searchResultArray);
    $errorMessage.innerText = '';
  } else {
    $teachersList.innerHTML = '';
    $errorMessage.innerText = 'Unfortunately, there is no teachers with this parametrs';
  }
});

$mapLink.addEventListener('click', () => {
  $teacherInfo.classList.toggle('toggled');
  $teacherMap.classList.toggle('displayed');
  $teacherContent.classList.toggle('hidden');
});

$arrowRight.addEventListener('click', () => {
  $favoritesList.scrollLeft += 220;
});

$arrowLeft.addEventListener('click', () => {
  $favoritesList.scrollLeft -= 220;
});

const handleAddForm = () => {
  $mainPage.classList.toggle('blur');
  $add.classList.toggle('displayed');
};

$closeAddForm.addEventListener('click', handleAddForm);
$teacherButtonForm[0].addEventListener('click', handleAddForm);
$teacherButtonForm[1].addEventListener('click', handleAddForm);

$closeInfoForm.addEventListener('click', () => {
  $mainPage.classList.toggle('blur');
  $teacherInfo.classList.toggle('displayed');
  $teacherInfo.classList.remove('toggled');
  $teacherContent.classList.remove('hidden');
});

$genderCheckbox.addEventListener('click', () => {
  $randomCheckbox.checked = false;
  $genderSelect.classList.toggle('displayed');
});

$countryCheckbox.addEventListener('click', () => {
  $randomCheckbox.checked = false;
  $countrySelect.classList.toggle('displayed');
});

$randomCheckbox.addEventListener('click', () => {
  $genderCheckbox.checked = false;
  $countryCheckbox.checked = false;
  $countrySelect.classList.remove('displayed');
  $genderSelect.classList.remove('displayed');
});

$dataSubmit.addEventListener('click', (e) => {
  e.preventDefault();
  if ($randomCheckbox.checked) {
    doQuery('https://randomuser.me/api/?results=50');
  } else {
    let queryString = '';
    if ($genderCheckbox.checked) {
      queryString += `gender=${$genderSelect.value}`;
    }
    if ($countryCheckbox.checked) {
      let country;
      switch ($countrySelect.value) {
        case 'Australia':
          country = 'AU';
          break;
        case 'Brasil':
          country = 'BR';
          break;
        case 'Canada':
          country = 'CA';
          break;
        case 'Switzerland':
          country = 'CH';
          break;
        case 'Germany':
          country = 'DE';
          break;
        case 'Denmark':
          country = 'DK';
          break;
        case 'Spain':
          country = 'ES';
          break;
        case 'Finland':
          country = 'FI';
          break;
        case 'France':
          country = 'FR';
          break;
        case 'United Kingdom':
          country = 'GB';
          break;
        case 'Ireland':
          country = 'IE';
          break;
        case 'Iran':
          country = 'IR';
          break;
        case 'Norway':
          country = 'NO';
          break;
        case 'Netherlands':
          country = 'NL';
          break;
        case 'New Zealand':
          country = 'NZ';
          break;
        case 'Turkey':
          country = 'TR';
          break;
        case 'United States':
          country = 'US';
          break;
        default:
      }
      queryString += `&nat=${country}`;
    }
    doQuery(`https://randomuser.me/api/?${queryString}&results=50`);
  }
});

let pageAdd = 2;

$listAddButton.addEventListener('click', () => {
  fetch(`https://randomuser.me/api/?page=${pageAdd}&results=10&seed=a`)
    .then((response) => response.json())
    .then((data) => {
      functions.normalizedJoin(data.results).forEach((element) => {
        const $teachersListItem = createListItem(element);
        if (element.favorite) {
          $favoritesList.appendChild($teachersListItem.cloneNode(true));
          $teachersListItem.classList.add('favorite');
        }
        $teachersList.appendChild($teachersListItem);
        $table.appendChild(createTableRow(element));
        teachers.push(element);
      });
      lastPage = Math.ceil(teachers.length / pageSize);
      $currentPage.innerText = `Page:${pageNumber}/${lastPage}`;
      paginateTable();
      pageAdd += 1;
    });
});

fetch('https://randomuser.me/api/?results=50&seed=a')
  .then((response) => response.json())
  .then((data) => {
    teachers = functions.normalizedJoin(data.results);
    initPageData(teachers);
  });

sortTableRow($tableName, 'full_name');
sortTableRow($tableAge, 'age');
sortTableRow($tableGender, 'gender');
sortTableRow($tableCountry, 'country');
